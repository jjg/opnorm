Opnorm
======

This is the opnorm package, a C implementation of S. W. Drury's
algorithm for finding the (_p_, _q_)-operator norm of matrices
with a small number of columns.  Bindings for Matlab, Octave
and Python are provided. For documentation and stable releases
see the [Stečkin homepage][1].

The implementation of the algorithm is largely based on the
C++ implementation by S.W. Drury available [here][2] and
described in the paper

>  S.W. Drury, "A counterexample to a conjecture of Matsaev",
>  Lin. Alg. Appl. **435** (2011), 323–329.

This reimplementation and the Octave, Matlab and Python bindings
are by J.J. Green.

Please see the file `COPYING.txt` for conditions under which you
can use, modify and redistribute this package.


Building
--------

To create the Python binding, run

    ./configure --enable-python
    make

Similarly for the Octave and Matlab bindings.

If you want to use the multi-threaded version (Unix only) then
add `--enable-pthread` to the configure options.  The resulting
modules will use the as many threads as there are processors
available (provided that it can determine the number). The number
of threads used can be set by assigning the environmental variable
`OPNORM_NTHREAD` appropriately, for example by

    export OPNORM_NTHREAD=4

under bash.

To use BLAS for the matrix-vector multiply, use the `--enable-blas`
option on configure; your BLAS installation needs to provide
the `cblas.h` header and the `cblas_dgemv()` function. The author
finds that there is not much difference between the Netlib
F77 version (built for Debian linux) and the built-in matrix-vector
multiply used if the `--enable-blas` option is omitted. Possibly
this is since the matrices are small.


Building the MEX on Windows
---------------------------

On non-POSIX systems such as Windows one may not have
the various tools (make,..) needed for the automated
build.  To generate the MEX library in this case, change
into the directory `src/mex` and run the file `build.bat`
in a DOS shell. (This has not been tested, please report
issues.)


Installing
----------

To install the Python or Octave bindings in the system
location (typically somewhere in `/usr/local`) run

    sudo make install

after building.

To install the Matlab MEX library you need to copy it to
somewhere that Matlab can find it (but there does not
seem to be a standard mechanism for doing this). The MEX
file will be

    src/mex/opnorm.<ext>

where the extension <ext> depends on your platform, `mexa64`
for Linux/AMD64, `mexw32` for Windows32 and so on.

You should copy the file

    src/mex/opnorm.m

(which contains the online documentation) to the same
location as the MEX file.


Bug reports
-----------

Please report any issues via [GitLab issues][3].

[1]: http://jjg.gitlab.io/en/code/steckin/
[2]: http://www.math.mcgill.ca/drury/research/matsaev/matsaev.html
[3]: https://gitlab.com/jjg/opnorm/issues
