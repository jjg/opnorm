# Makefile for opnorm

RUBBISH = *~
CONFFILES = config.cache config.log config.status
MAKE_SRC = $(MAKE) -C src

default: all

include Version.mk

# targets

all:
	$(MAKE_SRC) all

install:
	$(MAKE_SRC) install

test: unit accept

unit:
	$(MAKE_SRC) --quiet unit

accept:
	$(MAKE_SRC) --quiet accept

clean:
	$(MAKE_SRC) clean
	$(RM) $(RUBBISH)

spotless veryclean:
	$(MAKE_SRC) spotless
	$(RM) $(CONFFILES) $(RUBBISH)
	rm -rf autom4te.cache/

SCAN_RESULTS="/tmp/scan-build"

scan-build:
	scan-build -v ./configure
	scan-build -v -o $(SCAN_RESULTS) make

PKG = $(PACKAGE_VERSION)

git-create-tag:
	bin/git-release-tag $(PKG)

git-push-tag:
	git push origin v$(PKG)

DIST = opnorm-$(PKG)

dist:
	tar -C .. \
	  --transform s/^opnorm/$(DIST)/ \
	  --exclude-from .distignore \
	  -zpcvf ../$(DIST).tar.gz opnorm

.PHONY: dist
