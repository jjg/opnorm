function accept_drury_k()
% check value of the Drury K matrix with that found
% by the original OperatorNorm.cpp implementation
% when used with eps = 1e-14

    K = full(mmread('../mtx/drury/K.mtx'));
    [N, v, nevals] = opnorm(K, 4, 4, 1e-10);
    orig = 0.99999943874177;
    assert(abs(N - orig) / N < 1e-10, 'Drury K')
