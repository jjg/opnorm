function accept_hadamard()
% compare opnorm against the explict formula of the
% p-norm of Hadamard matrice described in the MO
% query: https://mathoverflow.net/questions/404029/
% Don't increase the size on N, it will kill your
% machine

  epsilon = 1e-10;
  N = 2;
  H = hadamard(N);
  ps = [2:10];
  for i = 1 : length(ps)
    p = ps(i);
    d1 = opnorm(H, p, p, epsilon);
    d2 = N^(1 - 1 / p);
    assert(abs(d1 - d2) / d2 < epsilon, 'hadamard')
  end
end
