function accept_maximises()
% confirm that the maximising vector actually
% gives the maximising value

  A = [1, 2, 3
       4, 5, 1
       2, 3, 4];
  epsilon = 1e-10;
  [d1, v] = opnorm(A, 3, 3, epsilon);
  d2 = norm(A * v, 3) / norm(v, 3);
  assert(abs(d1 - d2) / d2 < epsilon, 'maximises');
end
