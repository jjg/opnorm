% simple octave/matlab test harness

pattern = '';
warning('error', 'Octave:broadcast');

% this does not seem to work on Octave 3.2 on Travis
% [this_dir] = fileparts(mfilename('fullpath'))
this_dir = pwd();
test_dir = strcat(this_dir, '/', 'm');
addpath(test_dir);

err = 0;
tfiles = dir(strcat(test_dir, '/', 'accept_*', pattern, '.m'));
ntfiles = length(tfiles);

for i = 1:ntfiles

    tfile = tfiles(i).name;
    tfun = tfile(1:length(tfile)-2);

    try
        feval(tfun);
    catch
        fprintf('%s: %s\n', tfun, lasterr);
        L = lasterror;
        S = L.stack;
        for i = 1:numel(S)
            Si = S(i);
            fprintf('%s, line %i\n', Si.file, Si.line);
        end
        err = err+1;
    end

end

rmpath(test_dir);
fprintf('passed %i/%i tests\n', ntfiles-err, ntfiles);

quit;
