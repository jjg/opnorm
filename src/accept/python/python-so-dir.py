from sys import platform, version_info
from os import uname

print(
    'lib.%s-%s-%i.%i' % (
        platform,
        uname()[4],
        version_info[0],
        version_info[1]
    )
)
