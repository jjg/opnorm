/*
  Python interface to opnorm
  J.J. Green 2011, 2013, 2015
*/

#include <Python.h>

#define PY_ARRAY_UNIQUE_SYMBOL OPNORM
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <numpy/arrayobject.h>
#include <stdlib.h>

#include "opnorm.h"
#include "status.h"

static PyObject* opnorm_opnorm(PyObject *self, PyObject *args, PyObject *kwargs)
{
  PyArrayObject *A;
  double p, q, eps = 1e-10;
  size_t fifomax = 0;
  static char *kwlist[] = {"A", "p", "q", "eps", "fifomax", NULL};

  /* get arguments */

  if (! PyArg_ParseTupleAndKeywords(args, kwargs, "Odd|dn", kwlist,
				    &A, &p, &q, &eps, &fifomax) )
    {
      PyErr_SetString(PyExc_ValueError, "error parsing arguments");
      return NULL;
    }

  /* check and retrieve dimension */

  if (PyArray_NDIM(A) != 2)
    {
      PyErr_SetString(PyExc_ValueError, "First argument must be 2D array");
      return NULL;
    }

  size_t
    m = PyArray_DIM(A, 0),
    n = PyArray_DIM(A, 1);

  /* get matrix data */

  const double *Adat = PyArray_DATA(A);

  /* create array object for maximising vector */

  PyArrayObject *vmax;
  npy_intp dims[1] = {n};

  if (! (vmax = (PyArrayObject*)PyArray_SimpleNew(1, dims, NPY_DOUBLE)))
    {
      PyErr_SetString(PyExc_RuntimeError, "failed to create output array");
      return NULL;
    }

  /* get underlying data for vmax */

  double *vdat = (double*)PyArray_DATA(vmax);

  /* run opnorm */

  double N;
  opnorm_opt_t opt = { .eps = eps, .fifomax = fifomax };
  opnorm_stats_t stats;

  int err = opnorm(Adat, row_major, m, n, p, q, opt, &N, vdat, &stats);

  if (err)
    {
      const char *msg = opnorm_strerror(err);
      switch (err)
	{
	case OPNORM_EDOM_P:
	case OPNORM_EDOM_Q:
	case OPNORM_EDOM_EPS:
	  PyErr_SetString(PyExc_ValueError, msg);
	  break;
	default:
	  PyErr_SetString(PyExc_RuntimeError, msg);
	}
      return NULL;
    }

  return Py_BuildValue("fO{s:k,s:k,s:k,s:I}", N, vmax,
		       "neval", stats.neval,
		       "nfifo", stats.nfifo,
		       "fifomax", stats.fifomax,
		       "nthread", stats.nthread);
}

#if PY_MAJOR_VERSION >= 3

static PyMethodDef opnorm_methods[] = {
  {
    "opnorm", (PyCFunction)opnorm_opnorm, METH_VARARGS | METH_KEYWORDS,
    "opnorm(A, p, q, [eps=1e-10, fifomax=0]) -> (N, v, stats)\n"
    "\n"
    "Calculate the (p, q)-operator norm of a matrix A to relative\n"
    "accuracy eps.\n"
    "\n"
    "The return values are the norm N, a maximising vector v and a\n"
    "dictionary of statistics on the calculation: number of evaluations\n"
    "(matrix-vector multiplications), FIFO throughput and peak lenght\n"
    "and number of threads used."
  },
  {NULL, NULL, 0, NULL}
};

static struct PyModuleDef OpnormModuleDef = {
  PyModuleDef_HEAD_INIT,
  "opnorm",
  "The opnorm module",
  -1,
  opnorm_methods,
  NULL,
  NULL,
  NULL,
  NULL
};

PyMODINIT_FUNC PyInit_opnorm(void)
{
  Py_Initialize();
  import_array();
  return PyModule_Create(&OpnormModuleDef);
}

#else

/* description of exported methods */

static PyMethodDef OpnormMethods[] = {
  {
    "opnorm", (PyCFunction)opnorm_opnorm, METH_VARARGS | METH_KEYWORDS,
    "opnorm(A, p, q, [eps=1e-10, fifomax=0]) -> (N, v, stats)\n"
    "\n"
    "Calculate the (p, q)-operator norm of a matrix A to relative\n"
    "accuracy eps.\n"
    "\n"
    "The return values are the norm N, a maximising vector v and a\n"
    "dictionary of statistics on the calculation: number of evaluations\n"
    "(matrix-vector multiplications), FIFO throughput and peak lenght\n"
    "and number of threads used."
  },
  {NULL, NULL, 0, NULL}
};

/* module initialisation */

PyMODINIT_FUNC initopnorm(void)
{
  (void)Py_InitModule("opnorm", OpnormMethods);
  import_array();
}

#endif
