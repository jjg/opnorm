/*
  tests_pnorm.c
  Copyright (c) J.J. Green 2014
*/

#include <float.h>

#include <pnorm.h>
#include "tests_pnorm.h"
#include "helper.h"

CU_TestInfo tests_pnorm[] =
  {
    {"Pythagorean triples", test_pnorm_pythagorean_triples},
    {"Pythagorean quadruplets", test_pnorm_pythagorean_quadruplets},
    {"Faulhaber 6", test_pnorm_faulhaber6},
    {"Jacobi-Madden", test_pnorm_jacobi_madden},
    {"p = infinity", test_pnorm_infinity},
    CU_TEST_INFO_NULL,
  };

#define TEST_EPS (1.5 * DBL_EPSILON)

/*
  Pythagorean triples are a nice test for the 2-norm
*/

void test_pnorm_pythagorean_triples(void)
{
  int pts[16][3] = {
    { 3, 4, 5 },
    { 5, 12, 13},
    { 8, 15, 17},
    { 7, 24, 25},
    {20, 21, 29},
    {12, 35, 37},
    { 9, 40, 41},
    {28, 45, 53},
    {11, 60, 61},
    {16, 63, 65},
    {33, 56, 65},
    {48, 55, 73},
    {13, 84, 85},
    {36, 77, 85},
    {39, 80, 89},
    {65, 72, 97}
  };

  for (size_t i = 0 ; i < 16 ; i++)
    {
      int *pt = pts[i];
      double v[2] = {pt[0], pt[1]};

      CU_ASSERT( eq_rel(pnorm(v, 2, 2.0), pt[2], TEST_EPS) );
    }
}

/*
  Likewise for Pythagorean quadruplets
*/

void test_pnorm_pythagorean_quadruplets(void)
{
  int pqs[31][4] = {
    { 1,  2,  2,  3},
    { 2, 10, 11, 15},
    { 4, 13, 16, 21},
    { 2, 10, 25, 27},
    { 2,  3,  6,  7},
    { 1, 12, 12, 17},
    { 8, 11, 16, 21},
    { 2, 14, 23, 27},
    { 1,  4,  8,  9},
    { 8,  9, 12, 17},
    { 3,  6, 22, 23},
    { 7, 14, 22, 27},
    { 4,  4,  7,  9},
    { 1,  6, 18, 19},
    { 3, 14, 18, 23},
    {10, 10, 23, 27},
    { 2,  6,  9, 11},
    { 6,  6, 17, 19},
    { 6, 13, 18, 23},
    { 3, 16, 24, 29},
    { 6,  6,  7, 11},
    { 6, 10, 15, 19},
    { 9, 12, 20, 25},
    {11, 12, 24, 29},
    { 3,  4, 12, 13},
    { 4,  5, 20, 21},
    {12, 15, 16, 25},
    {12, 16, 21, 29},
    { 2,  5, 14, 15},
    { 4,  8, 19, 21},
    { 2,  7, 26, 27}
  };

  for (size_t i = 0 ; i < 31 ; i++)
    {
      int *pq = pqs[i];
      double v[3] = {pq[0], pq[1], pq[2]};

      CU_ASSERT( eq_rel(pnorm(v, 3, 2.0), pq[3], TEST_EPS) );
    }
}

/*
  Solutions to the Jacobi-Madden equation give some exact tests
  for the 4-norm
*/

void test_pnorm_jacobi_madden(void)
{
  int jms[2][5] = {
    { -2634,  955, 1770,   5400,  5491 },
    {-31764, 7590, 27385, 48150, 51361 }
  };

  int i;

  for (i=0 ; i<2 ; i++)
    {
      int *jm = jms[i];

      double v[4] = {jm[0], jm[1], jm[2], jm[3]};

      CU_ASSERT( eq_rel(pnorm(v, 4, 4.0), jm[4], TEST_EPS) );
    }
}

/*
  Faulhaber's formula gives exact values for

    1^k + 2^k + ... + n^k

  as a polynomial whose coefficients are expressed in term
  of the Bernoulli numbers; we parameterise by n for fixed
  k = 6 (this gives exact 6-norm examples of of vectors of
  arbitrary length).
*/

static unsigned long faulhaber6(unsigned long n)
{
  unsigned long n2 = n*n;

  return (((((6*n + 21)*n + 21)*n2 - 7)*n2 + 1)*n)/42;
}

void test_pnorm_faulhaber6(void)
{
  size_t max = 32;
  double v[max];

  for (size_t n = 0 ; n < max ; n++)
    v[n] = n+1;

  for (size_t n = 1 ; n < max ; n++)
    {
      double norm = pow(faulhaber6(n), 1/6.0);
      CU_ASSERT( eq_rel(pnorm(v, n, 6.0), norm, TEST_EPS) );
    }
}

/*
  infinity-norm is implemented seperately, so we have
  a separate test
*/

void test_pnorm_infinity(void)
{
  double v[5] = {1, 2, 3, 4, -5};

  CU_ASSERT( eq_rel(pnorm(v, 5, INFINITY), 5.0, TEST_EPS) );
}
