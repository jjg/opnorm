/*
  tests_cube.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_CUBE_H
#define TESTS_CUBE_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cube[];
void test_cube_halfwidth(void);

#endif
