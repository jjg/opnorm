/*
  tests_matvec.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_MATVEC_H
#define TESTS_MATVEC_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_matvec[];
void test_matvec_colmajor(void);
void test_matvec_rowmajor(void);

#endif
