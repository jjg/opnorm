/*
  tests_patch.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_PATCH_H
#define TESTS_PATCH_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_patch[];

void test_patch_corner_2d(void);

#endif
