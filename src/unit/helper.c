/*
  helper functions
*/

#include <math.h>
#include "helper.h"

/* a == b with relative error < eps */

bool eq_rel(double a, double b, double eps)
{
  return fabs((a-b)/b) < eps;
}
