/*
  helper.h
  Copyright (c) J.J. Green 2014
*/

#ifndef HELPER_H
#define HELPER_H

#include <stdbool.h>

bool eq_rel(double a, double b, double eps);

#endif
