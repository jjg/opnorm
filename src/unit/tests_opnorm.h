/*
  tests_opnorm.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_OPNORM_H
#define TESTS_OPNORM_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_opnorm[];
void test_opnorm_pq(void);
void test_opnorm_eps(void);
void test_opnorm_fifomax(void);
void test_opnorm_drury_k(void);
void test_opnorm_random_7x7(void);

#endif
