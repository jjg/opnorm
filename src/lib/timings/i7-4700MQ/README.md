i7-4700MQ
---------

Timings for a quad-core Intel® Core™ i7-4700MQ 2.5 GHz with
hyperthreading enabled (8 virtual CPUs).
