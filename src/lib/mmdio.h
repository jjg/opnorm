/*
  mmdio.h

  read ascii data from a stream, in particular from
  the data section of a matrix market file

  J.J. Green 2011
*/

#ifndef MMDIO_H
#define MMDIO_H

#include <stdio.h>
#include <stdlib.h>

/*
  callback function, will be called with the indices
  and the value (double, complex or integer) of each
  matrix entry in the stream, typically it will insert
  a value into the user defined data structure stored
  in the void*
*/

typedef int (*mmdr_cbr_t)(size_t, size_t, double, void*);
typedef int (*mmdr_cbc_t)(size_t, size_t, double, double, void*);
typedef int (*mmdr_cbi_t)(size_t, size_t, int, void*);

int mmdr_cr(FILE*, size_t, mmdr_cbr_t, void*);
int mmdr_ar(FILE*, size_t, size_t, mmdr_cbr_t, void*);

int mmdr_cc(FILE*, size_t, mmdr_cbc_t,void*);
int mmdr_ac(FILE*, size_t, size_t, mmdr_cbc_t, void*);

int mmdr_ci(FILE*, size_t, mmdr_cbi_t,void*);
int mmdr_ai(FILE*, size_t, size_t, mmdr_cbi_t, void*);

#endif
