/*
  trace.h

  debugging functions for mmnb

  Copyright (c) J.J. Green 2013
*/

#ifdef DEBUG_TRACE

#include <stdio.h>
#include <stddef.h>
#include "trace.h"

void cube_print(const cube_t *cube, size_t n, const char *prefix)
{
  printf("%s %8.6f %i | ", prefix, cube_halfwidth(cube), (int)cube->side);

  for (size_t i = 0 ; i < n ; i++)
    printf("%6.3f ", cube->centres[i]);

  printf("\n");
}

void patch_print(patch_t patch, index_t n)
{
  printf(" (%8.6f   | ", patch.radius);

  for (size_t i = 0 ; i < n ; i++)
    printf("%6.3f ", patch.centres[i]);

  printf(")\n");
}

#endif
