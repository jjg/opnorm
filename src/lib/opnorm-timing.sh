#! /bin/sh
#
# script to generate timing stats -- this takes about 20
# minutes to run, typical output is to be found in the
# timings subdirectories

TESTS=128
P=4.0
NMAX=7
THREADMAX=8

for nt in $(seq 1 $THREADMAX)
do
    export OPNORM_NTHREAD=$nt
    dat=$(printf "opnorm-timing-t%i.dat" $nt)
    rm -f $dat
    echo -n "$dat : "
    for n in $(seq 2 $NMAX)
    do
	echo -n "[$n] "
	./opnorm-timing $TESTS $n $P >> $dat
    done
    echo
done
