#include "mmdio.h"

int mmdr_cr(FILE *st, size_t nnz, mmdr_cbr_t f, void *opt)
{
  for (size_t i = 0 ; i < nnz ; i++)
    {
      size_t m, n;
      double v;

      if (fscanf(st, "%zi %zi %lf", &m, &n, &v) != 3)
	return 1;

      if ((m == 0) || (n == 0))
	return 1;

      int err = f(m-1, n-1, v, opt);

      if (err != 0) return err;
    }

  return 0;
}

int mmdr_ar(FILE *st, size_t m, size_t n, mmdr_cbr_t f, void *opt)
{
  for (size_t i = 0 ; i < m ; i++)
    {
      for (size_t j = 0 ; j < n ; j++)
	{
	  double v;

	  if (fscanf(st, "%lf", &v) != 1)
	    return 1;

	  int err = f(j, i, v, opt);

	  if (err != 0) return err;
	}
    }

  return 0;
}
