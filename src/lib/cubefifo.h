/*
  cubefifo.h

  A specialisation of FIFO for storing cubes

  Copyright (c) J.J. Green 2013
*/

#ifndef CUBEFIFO_H
#define CUBEFIFO_H

#include <stddef.h>

#include "fifo.h"

fifo_t* init_fifo(size_t n, size_t fifomax);
int empty_fifo(fifo_t *fifo);

#endif
