/*
  Calculate the operator norm of a matrix using the
  algorithm of S.W. Drury [1].

  J.J. Green, 2011, 2012
*/

/*
  References

  [1] S.W. Drury, "A counterexample to a conjecture of Matsaev",
  Lin. Alg. Appl., 435 (2011) 323-329.
*/

#define _POSIX_C_SOURCE 1

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "opnorm.h"
#include "fifo.h"
#include "lcrp.h"
#include "index.h"
#include "pnorm.h"
#include "matvec.h"
#include "patch.h"
#include "status.h"
#include "trace.h"
#include "cubefifo.h"
#include "unused.h"

#include <errno.h>
#include <string.h>
#include <math.h>
#include <float.h>

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

/* these are Lemmas 5 and 3 of [1], respectively */

static inline double radius_to_ratio(double rad, double p)
{
  return 1 - (p < 2 ?
	      pow(rad, p) :
	      0.5 * (p-1) * rad * rad);
}

/*
  signal handler for systems with signal.h
*/

#ifdef HAVE_SIGNAL_H

static int exit_flag = 0;
static void setexitflag(int UNUSED(sig)){ exit_flag = 1; }

#endif

/* the main function */

int opnorm(const double *M, majority_t maj,
           size_t m, size_t n,
           double p, double q,
           opnorm_opt_t opt,
           double *norm,
           double *vmax,
           opnorm_stats_t *pstats)
{
  if ( (p <= 1.0) || (isinf(p) == 1) )
    {
      errno = EDOM;
      return OPNORM_EDOM_P;
    }

  if ( q < 1.0 )
    {
      errno = EDOM;
      return OPNORM_EDOM_Q;
    }

  if ( opt.eps <= 0.0 )
    {
      errno = EDOM;
      return OPNORM_EDOM_EPS;
    }

  if ( opt.eps <= DBL_EPSILON )
    {
      return OPNORM_INACC;
    }

#ifdef HAVE_SIGNAL_H

  /* install handler for ctl-c */

  static struct sigaction act, oldact;

  act.sa_handler = setexitflag;
  act.sa_flags   = 0;
  sigemptyset(&act.sa_mask);

  exit_flag = 0;
  sigaction(SIGINT, &act, &oldact);

#endif

  /*
    choose matrix-vector multipy function depending
    on the specified matrix majority
  */

  void (*matvec)(const double*, const double*, index_t, index_t, double*);

  switch (maj)
    {
    case row_major:
      matvec = matvec_row_major;
      break;
    case column_major:
      matvec = matvec_column_major;
      break;
    default:
      return OPNORM_BUG;
    }

  /* initialise statistics struct */

  opnorm_stats_t stats =
    {
      .neval   = 0,
      .nfifo   = 0,
      .fifomax = 0,
      .nthread = 1
    };

  /* Lipschitz constant for the radial projection */

  double LCRP;

  if (lcrp(p, &LCRP) != 0)
    return OPNORM_BUG;

  /* initialise the cube queue */

  fifo_t *fifo;

  if ( !(fifo = init_fifo(n, opt.fifomax)) )
    return OPNORM_FIFO;

  /*
    surface cube diameter - the diameter of a (n-1)-cube
    of side one measured with the lp norm
  */

  double SCD = pow(n-1, 1/p);

  /* main loop */

  double  tmax = 0;
  double  pcent[n];
  cube_t  cube0, cube1;
  patch_t patch;

  patch.centres = pcent;

  int err = OPNORM_OK, err_fifo;

  while ( (err_fifo = fifo_dequeue(fifo, &cube0)) == FIFO_OK )
    {

#ifdef HAVE_SIGNAL_H

      /* check for ctl-c */

      if (exit_flag)
	{
	  err = OPNORM_USER;
	  break;
	}

#endif

      cube_print(&cube0, n, ">");

      /*
	nfifo is the throughput of the fifo, which is
	the same as the total number dequeued
      */

      stats.nfifo++;

      /* cube subdivide */

      int hwnexp = cube0.hwnexp + 1;
      double halfwidth = ldexp(1, -hwnexp);

      /*
	if halfwidth < DBL_EPSILON then we cannot
	calulate the centres of the subdivided cubes
	accurately, we break out and report that the
	requested accuracy could not be achieved
      */

      if (halfwidth < DBL_EPSILON)
	{
	  err = OPNORM_INACC;
	  break;
	}

      for (size_t k = 0 ; k < (1UL << (n-1)) ; k++)
	{
	  cube1.side = cube0.side;
	  cube1.hwnexp = hwnexp;

	  /*
	    we give our cube1 a temporary set of centres
	    while we evaluate and decide whether to jetison
	    or enqueue it, only if the latter do we make a
	    malloc and copy the temporary centres.  this
	    saves a *lot* of malloc/free pairs
	  */

	  double centres[n];
	  cube1.centres = centres;

	  size_t k0 = k;

	  for (size_t j = 0 ; j < n ; j++)
	    {
	      if (cube0.side == j)
		{
		  cube1.centres[j] = cube0.centres[j];
		  continue;
		}

	      cube1.centres[j] =
                cube0.centres[j] +
		((k0 % 2) ? halfwidth : -halfwidth);

	      k0 /= 2;
	    }

	  cube_print(&cube1, n, "<");

	  /* get the corresponding patch */

	  cube_to_patch(&cube1, n, p, LCRP, SCD, &patch);
	  patch_print(patch, n);

	  double ratio = radius_to_ratio(patch.radius, p);

	  /*
	    check for patch viability - this check almost
	    always succeeds (on Drury K, this is false in
	    80/164016 cases, so 0.05% of the time) very
	    small beer. Yet it introduces a branch point,
	    so one might think it worth removing. Timing
	    tests indicate that there is no speedup in
	    doing so, so we keep it.
	  */

	  if (ratio > 0)
	    {
	      /* evaluate M at patch centre */

	      double v[m];

	      matvec(M, pcent, m, n, v);

	      stats.neval++;

	      double t = pnorm(v, m, q);

	      /* update bound on operator norm */

	      if (t > tmax)
		{
		  tmax = t;

		  /* assign the maximising vector */

		  if (vmax)
		    memcpy(vmax, pcent, n*sizeof(double));
		}

	      /* test whether we can jettison this cube */

	      if (t < (tmax * ratio * (1 + opt.eps)))
		continue;
	    }

	  /*
	    we will enqueue this cube, so we need to
	    allocate and copy its temporary centres set
	  */

	  if (! (cube1.centres = malloc(n*sizeof(double))))
	    {
	      err = OPNORM_ALLOC;
	      break;
	    }

	  memcpy(cube1.centres, centres, n*sizeof(double));

	  if ((err_fifo = fifo_enqueue(fifo, &cube1)) != FIFO_OK)
	    {
	      free(cube1.centres);
	      break;
	    }
	}

      if (err || err_fifo) break;

      free(cube0.centres);
    }

#ifdef HAVE_SIGNAL_H

  /* restore old ctl-c handler */

  sigaction(SIGINT, &oldact, NULL);

#endif

  /* clean up the fifo */

  if (empty_fifo(fifo) != 0)
    err = OPNORM_BUG;
  else
    stats.fifomax = fifo_peak(fifo);

  fifo_destroy(fifo);

  /*
    merge fifo error state to final error state
  */

  if (err == OPNORM_OK)
    {
      switch (err_fifo)
	{
	case FIFO_EMPTY:
	  break;
	case FIFO_USERMAX:
	  err = OPNORM_FIFOMAX;
	  break;
	default:
	  err = OPNORM_FIFO;
	  break;
	}
    }

  if (err == OPNORM_OK)
    {
      if (norm) *norm = tmax;
      if (pstats) *pstats = stats;
    }

  return err;
}

/* just a wrapper around the function in status.c */

const char* opnorm_strerror(int err)
{
  return status_string(err);
}
