/*
  cube.h

  cubes on the boundary of the l-infinity ball

  Copyright (c) J.J. Green 2012
*/

#ifndef CUBE_H
#define CUBE_H

/*
  cube_t represent the sub-cubes on the sides of the
  l-infinity ball. On 32 bit machines this uses 8
  (2 + 2 + 4) bytes, so any further cheese-paring that
  we did on this would be lost by alignment.
*/

typedef struct
{
  unsigned short side;
  unsigned short hwnexp;
  double *centres;
} cube_t;

double cube_halfwidth(const cube_t*);

#endif
