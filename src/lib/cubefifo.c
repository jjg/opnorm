/*
  cubefifo.c

  Copyright (c) J.J. Green 2013
*/

#include "cubefifo.h"
#include "cube.h"

#define FIFO_INITIAL 1024

fifo_t* init_fifo(size_t n, size_t fifomax)
{
  fifo_t *fifo;

  if ( !(fifo = fifo_new(sizeof(cube_t), FIFO_INITIAL, fifomax)) )
    return NULL;

  cube_t cube = { .hwnexp = 0 };

  for (size_t i = 0 ; i < n ; i++)
    {
      cube.side = i;

      double parity[2] = {1, -1};

      for (int j = 0 ; j < 2 ; j++)
	{
	  if (! (cube.centres = calloc(n, sizeof(double))))
	    goto cleanup;

	  cube.centres[i] = parity[j];

	  if (fifo_enqueue(fifo, &cube) != FIFO_OK)
	    goto cleanup;
	}
    }

  return fifo;

 cleanup:

  empty_fifo(fifo);
  fifo_destroy(fifo);

  return NULL;
}

/* attempt to empty the fifo, return 0 if all went well */

int empty_fifo(fifo_t *fifo)
{
  cube_t cube;
  int err;

  while ( (err = fifo_dequeue(fifo, &cube)) == FIFO_OK )
    free(cube.centres);

  return (err == FIFO_EMPTY ? 0 : 1);
}
