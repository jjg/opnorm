/*
  status.h

  return status for opnorm

  Copyright (c) J.J. Green 2012
*/

#include <stdlib.h>

#include "status.h"

/* error messages for opnorm() return values */

typedef struct
{
  int err;
  const char *msg;
} msg_entry_t;

const char* status_string(int err)
{
  static const char nomsg[] = "unimplemented message";

  msg_entry_t *m, mtab[] =
    {
      { OPNORM_OK,      "Success" },
      { OPNORM_EDOM_P,  "Domain error (1 < p < infinity required)" },
      { OPNORM_EDOM_Q,  "Domain error (1 <= q required)" },
      { OPNORM_EDOM_EPS,"Domain error (0 < eps required)" },
      { OPNORM_INACC,   "Requested accuracy cannot be achieved" },
      { OPNORM_FIFO,    "Error in FIFO" },
      { OPNORM_ALLOC,   "Error allocating memory" },
      { OPNORM_USER,    "User interruption" },
      { OPNORM_THREAD,  "Thread error" },
      { OPNORM_BUG,     "Bug tripped, please report" },
      { OPNORM_FIFOMAX, "User specifed FIFO max-length exceeded" },
      { -1,             NULL}
    };

  for (m = mtab ; m->msg ; m++)
    if (m->err == err)
      return m->msg;

  return nomsg;
}
