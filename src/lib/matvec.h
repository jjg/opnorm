/*
  matvec.h

  matrix-vector multiply

  Copyright (c) J.J. Green 2012, 2019
*/

#ifndef MATVEC_H
#define MATVEC_H

#include "index.h"

void matvec_row_major(const double * restrict,
                      const double * restrict,
                      index_t, index_t, double*);

void matvec_column_major(const double * restrict,
                         const double * restrict,
                         index_t, index_t, double*);

#endif
