/*
   Matrix market header I/O library for ANSI C99

   This code is to read the headers (and just the headers)
   of matrix market files. It is intended as an update to
   the mmio.* files distributed by NIST

     http://math.nist.gov/MatrixMarket

   but with a rather different design:

   - the "typecode" of mmio is an array of character codes
     with various macros with intutive names to access and
     modify the values, here we use an "open" structure
     and enumerated types to achieve the same goal

   - the mmio library had partial support for reading and
     writing entire matrices (as well as the headers), we
     do not even attempt to do this.

   - use of size_t rather than int for matrix sizes

   - ...

   J.J. Green 2011
*/

#ifndef MMHIO_H
#define MMHIO_H

#include <stdio.h>
#include <stdlib.h>

enum mmh_coord_e { mmh_coordinate, mmh_array };
typedef enum mmh_coord_e mmh_coord_t;

enum mmh_data_e { mmh_real, mmh_complex, mmh_pattern, mmh_integer };
typedef enum mmh_data_e mmh_data_t;

enum mmh_storage_e { mmh_symmetric, mmh_general, mmh_skew, mmh_hermitian };
typedef enum mmh_storage_e mmh_storage_t;

typedef struct
{
  mmh_coord_t   coord;
  mmh_data_t    data;
  mmh_storage_t storage;
} mmh_code_t;

int mmh_is_valid(mmh_code_t);

int mmh_read_banner(FILE *f, mmh_code_t*);
int mmh_read_mtx_crd_size(FILE*, size_t*, size_t*, size_t*);
int mmh_read_mtx_array_size(FILE*, size_t*, size_t*);

int mmh_write_banner(FILE*, mmh_code_t);
int mmh_write_mtx_crd_size(FILE*, size_t, size_t, size_t);
int mmh_write_mtx_array_size(FILE*, size_t, size_t);

#define MMH_FREAD            1
#define MMH_FWRITE           2
#define MMH_EOF	             3
#define MMH_NOT_MTX          4
#define MMH_NO_HEADER        5
#define MMH_BAD_OBJECT_TYPE  6
#define MMH_BAD_COORD_TYPE   7
#define MMH_BAD_DATA_TYPE    8
#define MMH_BAD_STORAGE_TYPE 9
#define MMH_LONG_LINE        10

#endif
