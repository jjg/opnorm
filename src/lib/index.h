/*
  index.h

  Use "fast" integer type if available.

  Copyright (c) J.J. Green 2012, 2019
*/

#ifndef INDEX_H
#define INDEX_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_STDINT_H
#include <stdint.h>
#define index_t uint_fast16_t
#else
#define index_t unsigned
#endif

#endif
