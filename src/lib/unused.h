/*
  unused.h
  Copyright (c) J.J. Green 2017
*/

#ifndef UNUSED_H
#define UNUSED_H

#ifdef __GNUC__
#  define UNUSED(x) UNUSED_ ## x __attribute__((__unused__))
#else
#  define UNUSED(x) UNUSED_ ## x
#endif

#endif
