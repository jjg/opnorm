/*
  test program which, when called as

    opnorm-mtx <p> <q> <file>

  prints out the (p, q)-operator norm of the matrix in the
  "matrix market" format <file>
*/

#include <stdio.h>
#include <stdlib.h>

#include "mmhio.h"
#include "mmdio.h"
#include "opnorm.h"

/* portmanteau matrix struct for mmdio */

typedef struct
{
  size_t m, n;
  double *v;
} matrix_t;

/*
  callback for mmdio, assign the (i, j)th entry of the
  matrix M
*/

static int mmdr_cb(size_t i, size_t j, double v, matrix_t *M)
{
  size_t ncol = M->n;

  M->v[i*ncol + j] = v;

  return 0;
}

int main(int argc, char **argv)
{
  /* command line */

  if (argc != 4)
    {
      fprintf(stderr, "3 arguments required\n");
      return EXIT_FAILURE;
    }

  double p = atof(argv[1]), q = atof(argv[2]);
  const char* path = argv[3];

  /* read matrix */

  FILE* st = fopen(path, "r");

  if ( !st )
    {
      fprintf(stderr, "failed fopen of %s\n", path);
      return EXIT_FAILURE;
    }

  mmh_code_t code;

  if (mmh_read_banner(st, &code) != 0)
    {
      fprintf(stderr, "failed read matrix market banner of %s\n", path);
      return EXIT_FAILURE;
    }

  matrix_t M;

  if (code.coord == mmh_coordinate)
    {
      size_t nnz;

      if (mmh_read_mtx_crd_size(st, &(M.m), &(M.n), &nnz) != 0)
        return EXIT_FAILURE;

      if (code.data == mmh_real)
        {
	  if ((M.v = calloc(M.m * M.n, sizeof(double))) == NULL)
	    return EXIT_FAILURE;

          if (mmdr_cr(st, nnz, (mmdr_cbr_t)mmdr_cb, &M) != 0)
            return EXIT_FAILURE;
        }
      else
	{
	  fprintf(stderr, "only real mmx supported so far\n");
	  return EXIT_FAILURE;
	}
    }
  else
    {
      fprintf(stderr, "only coordinate (sparse) mmx supported so far\n");
      return EXIT_FAILURE;
    }

  printf("read %zix%zi matrix\n", M.m, M.n);

  /* print matrix */

  size_t i, j;

  printf("[\n");
  for (i=0 ; i<M.m ; i++)
    {
      for (j=0 ; j<M.n ; j++)
	{
	  printf("%7.4f ", M.v[M.n*i+j]);
	}
      printf("\n");
    }
  printf("]\n");

  /* run opnorm */

  opnorm_opt_t opt;
  opnorm_stats_t stats;
  double norm;
  double vmax[M.n];

  opt.eps = 1e-10;
  opt.fifomax = 0;

  int err = opnorm(M.v, row_major, M.m, M.n, p, q, opt,
		   &norm, vmax, &stats);

  if (err)
    {
      fprintf(stderr, "opnorm failed: %s\n", opnorm_strerror(err));
      return EXIT_FAILURE;
    }

  printf("norm %14.12f maximised at\n", norm);
  printf("[\n");

  for (j=0 ; j<M.n ; j++)
    printf("  %15.12f\n", vmax[j]);

#define FIFOELB (2 * sizeof(unsigned short) + sizeof(double*))

  printf("]\n");
  printf("evaluations %lu\n", stats.neval);
  printf("fifo throughput %lu, peak length %lu (%lu kB)\n",
	 stats.nfifo, stats.fifomax,
	 (stats.fifomax * (FIFOELB + M.n * sizeof(double))) / 1024
	 );

  /* tidy up */

  free(M.v);

  fclose(st);

  return EXIT_SUCCESS;
}
