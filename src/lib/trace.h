/*
  trace.h

  debugging functions for mmnb

  Copyright (c) J.J. Green 2013
*/

#ifndef TRACE_H
#define TRACE_H

#ifdef DEBUG_TRACE

#include "cube.h"
#include "patch.h"

void cube_print(const cube_t *cube, size_t n, const char *prefix);
void patch_print(patch_t patch, index_t n);

#else

#define cube_print(cube, n, prefix)
#define patch_print(patch, n)

#endif

#endif
