function [N, v, stats] = opnorm(A, p, q, epsilon, fifomax)
%OPNORM   The operator norm of a real matrix
%
%   N = OPNORM(A, P, Q, EPSILON, FIFOMAX) returns the operator norm 
%   N of a real m x n matrix A considered as an operator
%
%     A : (R^m, ||.||p) -> (R^n, ||.||q)
%
%   using global optimisation on the (R^m, ||.||p) unit ball. The
%   result is returned to the requested accuracy EPSILON. If
%   omitted, Q will taken to be P, EPSILON to be 1e-10 and FIFOMAX
%   to be zero (no limit on fifo size).
%
%   [N, V, STATS] = OPNORM(A, P, Q, EPSILON, FIFOMAX) also returns a 
%   maximising vector V and a structure STATS with elements 
%   NEVAL   - the number of evaluations (matrix-vector multiplications)
%   NFIFO   - the throughput of the fifo
%   FIFOMAX - the maximum size of the fifo
%   NTHREAD - the number of threads used
%
%   The algorithm, due to S. W. Drury, is exponential in the number of
%   columns of the matrix A (typically 8 or 9 is the limit) but linear
%   in the number of rows (several thousands are possible). Note also
%   that the method slows as P approches one.  The parameters must
%   satisfy 1 < P < Inf and 1 <= Q <= Inf

% This file is only for the opnorm documentation, it will only
% be used "in anger" if the mex file is not found

   error ("opnorm: mex file not found");

end 
