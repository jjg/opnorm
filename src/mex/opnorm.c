/*
  opnorm.c
  MATLAB-C (MEX) interface for opnorm()

  J.J. Green 2011, 2015
*/

#include <mex.h>
#include <stdint.h>

#include "opnorm.h"

static int is_real_double(const mxArray *p)
{
  return mxIsDouble(p) && (!mxIsComplex(p));
}

static int is_scalar(const mxArray *p)
{
  return mxGetNumberOfElements(p) == 1;
}

/*
  return a 1x1 array of uint32/64 with the sole element
  assigned to i.  Note that this needs stdint.h (which
  is a C99 feature) in order to use the explict uint64
  used by the matlab function
*/

static mxArray* uint32_mxArray(unsigned int i)
{
  mxArray *val;

  if (! (val = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL)))
    return NULL;
  
  void *valdat;

  if (! (valdat = mxGetPr(val)))
    return NULL;

  *((uint32_t*)valdat) = i;

  return val;
}

static mxArray* uint64_mxArray(unsigned long i)
{
  mxArray *val;

  if (! (val = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL)))
    return NULL;
  
  void *valdat;

  if (! (valdat = mxGetPr(val)))
    return NULL;

  *((uint64_t*)valdat) = i;

  return val;
}

/*
  create a matlab struct array (1x1) which mirrors the
  opnorm stats struct
*/

static mxArray* stats_mxArray(opnorm_stats_t stats)
{
  const mwSize dims[] = {1};
  const char* fields[] = {"neval", "nfifo", "fifomax", "nthread"};

  mxArray *mxstats;

  if ((mxstats = mxCreateStructArray(1, dims, 4, fields)) == NULL)
    return NULL;

  mxSetField(mxstats, 0, "neval",   uint64_mxArray(stats.neval));
  mxSetField(mxstats, 0, "nfifo",   uint64_mxArray(stats.nfifo));
  mxSetField(mxstats, 0, "fifomax", uint64_mxArray(stats.fifomax));
  mxSetField(mxstats, 0, "nthread", uint32_mxArray(stats.nthread));

  return mxstats;
}

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
  /* check number of inputs/outputs */

  if (nrhs < 2) 
    {
      mexErrMsgIdAndTxt("opnorm:opnorm:nargin",
			"At least two inputs required");
    }

  if (nrhs > 5) 
    {
      mexErrMsgIdAndTxt("opnorm:opnorm:nargin",
			"At most five inputs allowed");
    }

  if (nlhs > 3) 
    {
      mexErrMsgIdAndTxt("opnorm:opnorm:nargout",
			"At most three outputs");
    }

  /* check and retrieve inputs */

  double *Adat, p, q;
  opnorm_opt_t opt;

  /* the matrix A */

  if (! is_real_double(prhs[0]) )
    mexErrMsgIdAndTxt("opnorm:opnorm:not_real_double",
		      "A must be a real double matrix");
  Adat = mxGetPr(prhs[0]);

  /* p */

  if ( (! is_real_double(prhs[1])) || (! is_scalar(prhs[1])))
    mexErrMsgIdAndTxt("opnorm:opnorm:not_real_double_scalar",
			  "p must be a real double scalar");
  p = mxGetScalar(prhs[1]);

  /* q */

  if (nrhs > 2)
    {
      if ( (! is_real_double(prhs[2])) || (! is_scalar(prhs[2])))
	mexErrMsgIdAndTxt("opnorm:opnorm:not_real_double_scalar",
			  "q must be a real double scalar");
      q = mxGetScalar(prhs[2]);
    }
  else
    {
      q = p;
    }

  /* epsilon */

  if (nrhs > 3)
    {
      if ( (! is_real_double(prhs[3])) || (! is_scalar(prhs[3])))
	mexErrMsgIdAndTxt("opnorm:opnorm:not_real_double_scalar",
			  "epsilon must be a real double scalar");
      opt.eps = mxGetScalar(prhs[3]);
    }
  else
    {
      opt.eps = 1e-10;
    }

  /* fifomax */

  if (nrhs > 4)
    {
      if ( ! is_scalar(prhs[4]) )
	mexErrMsgIdAndTxt("opnorm:opnorm:not_scalar",
			  "fifomax epsilon must be a scalar");
      opt.fifomax = mxGetScalar(prhs[4]);
    }
  else
    {
      opt.fifomax = 0UL;
    }

  /* matrix size */

  mwSize 
    n = mxGetN(prhs[0]),
    m = mxGetM(prhs[0]);
    
  /* prepare output data */

  mxArray *N = mxCreateDoubleMatrix(1, 1, mxREAL);
  double *Ndat = mxGetPr(N);

  mxArray *v = NULL;
  double  *vdat = NULL;

  if (nlhs > 1)
    {
      v = mxCreateDoubleMatrix(n, 1, mxREAL);
      vdat = mxGetPr(v);
    }

  opnorm_stats_t stats = {0};

  /* run opnorm */

  int err = opnorm(Adat, column_major, m, n, p, q, opt, 
		   Ndat, vdat, &stats);

  /* check for error */

  if (err)
    mexErrMsgIdAndTxt("opnorm:opnorm:external", 
		      opnorm_strerror(err));

  /* convert stats to matlab format */

  mxArray *mxstats = stats_mxArray(stats);
  
  if (! mxstats)
    mexErrMsgIdAndTxt("opnorm:opnorm:external",
		      "failed to create Matlab stats array");

  /*
    one would expect that this would not be wise to ignore
    the nlhs variable in and assign tke plhs pointers anyway,
    but apparently it is (and the first pointer is used for
    the matlab ans) 
  */

  plhs[0] = N;
  plhs[1] = v;
  plhs[2] = mxstats;
}
