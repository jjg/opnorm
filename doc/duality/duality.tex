\documentclass[10pt,a4paper]{article}
\usepackage{xspace}
\usepackage{amstext}
\usepackage{booktabs}
\usepackage{graphicx}

\title{Using duality in calculating operator norms}
\author{J.J. Green}
\date{14th July 2011}

\newcommand{\packagename}[1]{\texttt{#1}\xspace}
\newcommand{\opnorm}{\packagename{opnorm}}
\newcommand{\norm}[2]{\left\| #1 \right\|_{#2}}
\newcommand{\field}[1]{\mathbf{#1}}
\newcommand{\R}{\field{R}}
\newcommand{\order}[1]{O\left(#1\right)}

\begin{document}
\maketitle

The \opnorm package implements the algorithm of S.\,W.~Drury
to calculate the $(p,q)$-operator norm of an $m \times n$ 
real matrix $A$
\[
\norm{A}{p,q} = 
\max\left\{ 
\norm{Ax}{q} : 
\text{$x \in \R^n$, $\norm{x}{p} \leq 1$} 
\right\},
\]
where $\norm{x}{p}$ denotes the usual $\ell^p$-norm on vectors,
$1<p<\infty$ and $1\leq q\leq\infty$.

The algorithm relies on a method of global optimisation on the 
$\ell^p$ unit ball in $\R^n$ using a repeated subdivision and
rejection of non-maximising regions. As such, it inevitably 
requires space and time exponential in $n$, the number of 
columns of the matrix $A$ (in fact $\order{n2^n}$), typically
six or seven is the maximum feasible value. Moreover, the 
rejection criterion is inferior for $p<2$ and becomes useless
as $p$ approaches one.  

In some circumstances one can apply Banach-space duality
\[
\norm{A}{p,q} = \norm{A^T}{q',p'}
\]
where $A^T$ is the transpose of $A$, $p'$ is the conjugate of
$p$, i.e.,
\[
\frac{1}{p} + \frac{1}{p'} = 1
\]
and $q'$ is the conjugate of $q$. The point of this is that the
\opnorm algorithm is linear in $m$, the number of rows of $A$, so 
that if $A$ has more columns than rows then the opposite is true
for $A^T$ and then applying the algorithm to the dual problem 
is usually much more efficient.  The case when problems arise is
when the norm on the dual range space (i.e., the value of $q'$)
is close to one, corresponding to $q$ being large. As mentioned
above, the rejection criterion becomes useless as the norm on
the range space approaches one, very few regions are rejected
and the subdivision of the remainder quickly exhausts memory.

Thus, as a general rule, use duality if the matrix has many 
more columns than rows.  If the matrix has the same number of
rows as columns (or the difference is small) then try duality
if $p$ is close to one and if $q$ is not large.  

\begin{figure*}
  \begin{center}
    \includegraphics[width=3in]{evals}
  \end{center}
  \caption{The number of matrix-vector multiplications used
  to calculate the $(p,p)$-operator norm of a particular 
  $4\times3$ matrix for a range of values of $p$ directly
  (in black) and using duality (in red).}
\end{figure*}

\end{document}
